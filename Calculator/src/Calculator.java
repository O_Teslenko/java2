public class Calculator {
    PrepareString ps = new PrepareString();
    MathOperation mo = new MathOperation();
    private String inputString;

    public Calculator(String inputString) {
        this.inputString = inputString;
    }

    String Calculating(){
        return mo.MathOperation(ps.substrString(inputString));
    }

}
