import java.util.ArrayList;

public class MathOperation {

    String MathOperation(ArrayList al) {
        if (isString(al)){
            double result = 0;
            double arg1 = Double.parseDouble((String) al.get(0));
            double arg2 = Double.parseDouble((String) al.get(1));
            Object symbol = al.get(2);

            if (symbol.toString().equals("+")) result = arg1 + arg2;
            if (symbol.toString().equals("-")) result = arg1 - arg2;
            if (symbol.toString().equals("*")) result = arg1 * arg2;
            if (symbol.toString().equals("/")) result = arg1 / arg2;

            return isDouble(result);
        } else return "ERROR";
    }

    String isDouble(double d){
        String str = String.valueOf(d);
        int index = str.lastIndexOf(".");
        index = Integer.parseInt(str.substring(index+1,str.length()));

        if (index > 0) return String.valueOf(d);
        else return  String.valueOf(Integer.parseInt(str.substring(0, str.indexOf("."))));
    }

    boolean isString(ArrayList al){
        try {
            Double.parseDouble((String)al.get(0));
            Double.parseDouble((String)al.get(1));
        } catch (Exception e){
            return false;
        }
        return true;
    }
}
