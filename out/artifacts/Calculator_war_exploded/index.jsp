<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
      <script type="application/javascript" language="javascript" src="js/jquery-1.5.1.js"></script>
      <script type="application/javascript" language="javascript" src="js/buttons.js"></script>
    <title></title>
  </head>
  <body>
   <div>
       <form action="/Servlet" method="post">
           <table>
               <tr>
                   <td colspan="4"><input type="text" width="200" name="input_field" value="<%=request.getAttribute("rez") == null ? "" : request.getAttribute("rez")%>" /></td>
               </tr>
               <tr>
                   <td><input type="button" value="1" /></td>
                   <td><input type="button" value="2" /></td>
                   <td><input type="button" value="3" /></td>
                   <td><input type="button" value="+" /></td>
               </tr>
               <tr>
                   <td><input type="button" value="4" /></td>
                   <td><input type="button" value="5" /></td>
                   <td><input type="button" value="6" /></td>
                   <td><input type="button" value="-" /></td>
               </tr>
               <tr>
                   <td><input type="button" value="7" /></td>
                   <td><input type="button" value="8" /></td>
                   <td><input type="button" value="9" /></td>
                   <td><input type="button" value="*" /></td>
               </tr>
               <tr>
                   <td><input type="button" value="." /></td>
                   <td><input type="button" value="0" /></td>
                   <td><input type="submit" value="=" /></td>
                   <td><input type="button" value="/" /></td>
               </tr>
               <tr>
               		<td colspan="2"><input type="button" name="reset" value="C"></td>	
               </tr>
           </table>
       </form>
   </div>
  </body>
</html>