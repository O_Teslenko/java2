import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@javax.servlet.annotation.WebServlet(name = "Manager", urlPatterns = "/Manager")
public class Manager extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        managerClass(req, res);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        managerClass(req, res);
    }

    private void managerClass(HttpServletRequest req, HttpServletResponse res) throws IOException {
            ConstructorHTML.htmlPage(req, res);
    }
}
