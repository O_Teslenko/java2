import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ConstructorHTML {
    public static void htmlPage(HttpServletRequest req, HttpServletResponse res) throws IOException {
        res.setContentType("text/html; charset=utf-8;");
        PrintWriter out = res.getWriter();
        out.print("<html><head><title></title></head><body>");
        out.print(ContentConstructor.generator(req, res));
        out.print("</body></html>");
    }
}
