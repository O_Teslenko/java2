import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@javax.servlet.annotation.WebServlet(name = "Servlet", urlPatterns = "/Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String inputString = request.getParameter("input_field");
        Calculator calculator = new Calculator(inputString);

        response.setContentType("text/html");

        String str = calculator.Calculating();

        request.setAttribute("rez", str);
        request.getRequestDispatcher("index.jsp").forward(request,response);
    }
}
