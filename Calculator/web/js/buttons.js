	$(document).ready(function(e) {
    	$('input[type=button]').not($("input[name=reset]")).click(function(e) {
        	var val = $(this).val();
			var window_val = $("input[name=input_field]").val();
			$("input[name=input_field]").val(window_val + val);
        });
		$("input[name=reset]").click(function(e) {
        	$("input[name=input_field]").val("");
        });
	});