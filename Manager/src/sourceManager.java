import javax.servlet.http.HttpServletRequest;
import java.io.*;

public class SourceManager {
    public static String linkPath(File f){
        return f.getPath().substring(46);
    }

    public static String path(File f, HttpServletRequest req){
        StringBuffer sb = new StringBuffer();
        sb.append("<a href='?path=" + linkPath(f) + "&flag=create'>Create</a><br>");
        sb.append("<a href='?path='>" + req.getServletPath() + "</a>");
        if (linkPath(f).indexOf('\\') != -1){
            String mas[] = linkPath(f).replaceAll("\\\\", "/").split("/");

            String pathBack = "";
            for (int i = 0; i < mas.length; i++){
                pathBack += mas[i] + "\\";
                sb.append("<a href='?path=" + pathBack + "'>" + mas[i] + "</a>" + " / ");
            }
            sb.append("<br>");
        } else sb.append(" / <br>");
        return sb.toString();
    }

    public static String readFile(File f){
        StringBuffer sb = new StringBuffer();
        try {
            BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()));
            try{
                String s;
                while ((s = br.readLine()) != null){
                    sb.append(s);
                }
            } finally {
                br.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return sb.toString();
    }

    public static String dirList(File f, String path){
        StringBuffer sb = new StringBuffer();
        String[] list = f.list();

        for (int i = 0; i < list.length; i++){
            File f1 = new File(path + "/" + list[i]);
            if (f1.isDirectory()) sb.append("<a href='?path=" + linkPath(f1) + "'>" + list[i] + "</a><br>");
            else sb.append("<a href='?path=" + linkPath(f1) + "'>" + list[i] + "</a> " +
                    "<a href='?path=" + linkPath(f1) + "&flag=change'>Change</a> " +
                    "<a href='?path=" + linkPath(f1).replace(f1.getName(), "") + "&delete=true" + "&delname=" + f1.getName() + "'>Delete</a><br>");
        }
        return sb.toString();
    }

    public static boolean writeFile(File f, HttpServletRequest req){
        try {
            if (!f.exists()){
                f.createNewFile();
            }
            PrintWriter pw = new PrintWriter(f.getAbsolutePath());
            try {
                pw.print(req.getParameter("changeArea"));
            } finally {
                pw.close();
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static void deleteFile(File f, HttpServletRequest req){
        File f1 = new File(f.getPath() + "/" + req.getParameter("delname"));
        f1.delete();
    }

    public static void createFile(File f, HttpServletRequest req){
        File f1 = new File(f.getPath() + "/" + req.getParameter("newfile") + ".txt");
        try {
            f1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
