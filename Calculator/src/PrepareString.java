import java.util.ArrayList;

public class PrepareString {

    ArrayList substrString(String inputString){
        ArrayList al = new ArrayList();
        int index = mathArg(inputString);

        if (index != -1){
            char mathArg = inputString.charAt(index);
            String newStr = inputString.replace(mathArg, ' ');
            String[] s = newStr.split(" ");

            al.add(s[0]);
            al.add(s[1]);
            al.add(mathArg);
        }
        return al;
    }

    int mathArg(String inputString){
        int index = -1;
        char[] arg = {'+','-','*','/'};
        for (int i = 0; i < arg.length; i++){
            if (inputString.indexOf(arg[i]) != -1){
                index = inputString.indexOf(arg[i]);
            }
        }
        return index;
    }
}
