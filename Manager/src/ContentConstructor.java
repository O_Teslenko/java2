import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

public class ContentConstructor {
    public static String generator(HttpServletRequest req, HttpServletResponse res) {
        SourceManager sm = new SourceManager();
        String path = "C:/Users/Олег/IdeaProjects/java2/Manager/files";
        StringBuffer sb = new StringBuffer();
        if (req.getParameter("path") != null) path += "/" + req.getParameter("path");

        File f = new File(path);
        sb.append(sm.path(f, req));
        if (req.getParameter("update") != null && req.getParameter("update").equals("true")){
            if (!f.isDirectory()){
                if (sm.writeFile(f, req)) sb.append("<font color='red'>Success</font> <br>");
                else sb.append("<font color='red'>Fail</font> <br>");
            }
        }
        if (req.getParameter("delete") != null &&  req.getParameter("delete").equals("true")){
            sm.deleteFile(f, req);
        }
        if (req.getParameter("create") != null &&  req.getParameter("create").equals("true")){
            sm.createFile(f, req);
        }

        if (req.getParameter("flag") == null){
            if (f.isDirectory()){
                sb.append(sm.dirList(f, path));
            }else{
                sb.append(sm.readFile(f));
            }
        } else if (req.getParameter("flag").equals("change")){
            sb.append("<form action='/Manager?path=" + sm.linkPath(f) + "&update=true' method='post'>");
            sb.append("<textarea name='changeArea'>");
            sb.append(sm.readFile(f));
            sb.append("</textarea><br>");
            sb.append("<input type='submit' value='Change' />");
            sb.append("</form>");
        } else if (req.getParameter("flag").equals("create")){
            sb.append("<form action='/Manager?path=" + sm.linkPath(f) + "&create=true' method='post'>" +
                    "File name:<br>" +
                    "<input type='text' name='newfile' /><br>" +
                    "<input type='submit' value='Create' />" +
                    "</form>");
        }

        return sb.toString();
    }


}